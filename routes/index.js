const express = require('express');

const router = express.Router();

const { getTodos, createTodo, updateTodo ,deleteTodo} = require('../controller/todo');


router.get('/todos', getTodos);

router.post('/todo', createTodo);

router.put('/todo/:id', updateTodo);

router.delete('/todo/:id', deleteTodo);

module.exports = {
    router: router,

}