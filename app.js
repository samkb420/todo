require('dotenv').config()
const express = require("express");
const cors = require('cors');

const app = express();
app.use(cors());

app.use(express.json());

const todoRoutes = require('./routes/index');


app.get('/', (req, res) => {
    res.json({ message: 'Welcome to Todos API' });
});


app.use('/api/v1',todoRoutes.router );



const port = process.env.PORT || 3000;



app.listen(port, () => {
  console.log(`Server is up on ${port}`);
});