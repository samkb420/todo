const express = require('express');

const connectDB = require('../db/db');
const Todo = require('../model/todo');

connectDB();


const getTodos = async (req, res) => {
    try {
        const todos = await Todo.find();
        res.status(200).json({ 
            message: 'Todos retrieved successfully',
            todos
         });
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
    }

const createTodo = async (req, res) => {
    const { title, description } = req.body;
    const newTodo = new Todo(
        {
            title,
            description
        }
    );
    try {
        await newTodo.save();
        res.status(201).json(
            {
                message: 'Todo created successfully',
                newTodo
            }
        );
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
    }

const updateTodo = async (req, res) => {
  

    const todo = await Todo.findById(req.params.id);
    
    
    
   
    todo.completed = req.body.completed;

    
    try {
        await todo.save();
        res.status(201).json(
            {
                message: 'Todo updated successfully',
                todo
            }
        );
    }
    catch (error) {
        res.status(409).json({ message: error.message });
    }


}

const deleteTodo = async (req, res) => {
    const { id } = req.params;
    await Todo.findByIdAndRemove(id);
    res.json(
        {
            message: 'Todo deleted successfully'
        }
    );

}

module.exports = {
    getTodos,
    createTodo,
    updateTodo,
    deleteTodo

}

    





