/**
 * This file is used to configure the database connection.
 */

// load env file

require('dotenv').config();

const mongoose = require('mongoose');

const MONGODB_URL = process.env.MONGODB_URL;



const connectDB = async () => {
    try {
        await mongoose.connect(MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
       

        });
        console.log('MongoDB Connected');
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
    };


module.exports = connectDB;
